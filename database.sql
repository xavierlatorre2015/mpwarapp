CREATE DATABASE mpwar_frameworks CHARACTER SET utf8 COLLATE utf8_general_ci;

USE mpwar_frameworks;

DROP TABLE wines IF NOT EXIST;
CREATE TABLE wines (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
name VARCHAR (255)
)ENGINE=InnoDB;


INSERT INTO wines VALUES (0, "Cune");
INSERT INTO wines VALUES (0, "Marques de airenzo");
INSERT INTO wines VALUES (0, "Abadia Retuerta");
INSERT INTO wines VALUES (0, "Marques de riscal");