<?php
namespace Mpwarapp\home\controller;

use Mpwarfwk\Component\Response\ResponseHttp;
use Mpwarfwk\Component\Response\ResponseJson;
use Mpwarfwk\Component\Response\ResponseTwig;
use Mpwarfwk\Component\Container\Container;
use Mpwarfwk\Component\Controller\ControllerBase;

class TwigController extends ControllerBase
{
	private $templateSystem;

	public function __construct(Container $container)
	{
		parent::__construct($container);
		$this->templateSystem = $this->container->getService('twig_template');
	}

	public function index()
	{
		$this->templateSystem->assignVar('wine', 'Marques de Arienzo');
		$response = new ResponseTwig($this->templateSystem->createView('simpleWine.twig'));

		return $response;
	}
}
