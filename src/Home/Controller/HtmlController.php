<?php
namespace Mpwarapp\home\controller;

use Mpwarfwk\Component\Response\ResponseHttp;
use Mpwarfwk\Component\Response\ResponseJson;
use Mpwarfwk\Component\Response\ResponseTwig;
use Mpwarfwk\Component\Container\Container;
use Mpwarfwk\Component\Controller\ControllerBase;

class HtmlController extends ControllerBase
{
	public function __construct(Container $container)
	{
	}

	public function index()
	{
		return new ResponseHttp("This is not exisiting function wineHtml.");
	}

	public function contFunc()
	{
		$result = 'Simple response for wineHtml.';

		$response = new ResponseHttp($result);
		return $response;
	}
}
