<?php
namespace Mpwarapp\home\controller;

use Mpwarfwk\Component\Response\ResponseHttp;
use Mpwarfwk\Component\Response\ResponseJson;
use Mpwarfwk\Component\Response\ResponseTwig;
use Mpwarfwk\Component\Container\Container;
use Mpwarfwk\Component\Controller\ControllerBase;

class JsonController extends ControllerBase
{
	private $template_engine;

	public function __construct(Container $container)
	{
	}

	public function index()
	{
		$result = ['Respuesta JSON' => ['ejemplo'=>'Abadia Retuerta','ano'=>'2012']];
		$response = new ResponseJson($result);
		return $response;
	}
}
