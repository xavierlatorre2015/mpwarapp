<?php
namespace Mpwarapp\home\controller;

use Mpwarfwk\Component\Response\ResponseHttp;
use Mpwarfwk\Component\Response\ResponseJson;
use Mpwarfwk\Component\Response\ResponseTwig;
use Mpwarfwk\Component\Container\Container;
use Mpwarfwk\Component\Controller\ControllerBase;

class PdoController extends ControllerBase
{
	private $templateSystem;

	public function __construct(Container $container)
	{
		parent::__construct($container);
		$this->templateSystem = $this->container->getService('twig_template');
	}

	public function index()
	{
		$pdo_connection = $this->container->getService('pdo_connection');
		$pdo_connection->execute("INSERT INTO wines(id, name) VALUES(0, :name)", array('name' => 'Coppola'));
		$res = $pdo_connection->execute("SELECT id, name FROM wines WHERE name=:name", array('name' => 'Coppola'));
		$this->templateSystem->assignVar('wine', $res[0]['name']);

		return new ResponseTwig($this->templateSystem->createView('simpleWine.twig'));
	}
}
