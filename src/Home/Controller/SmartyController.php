<?php
namespace Mpwarapp\home\controller;

use Mpwarfwk\Component\Response\ResponseHttp;
use Mpwarfwk\Component\Response\ResponseJson;
use Mpwarfwk\Component\Response\ResponseSmarty;
use Mpwarfwk\Component\Container\Container;
use Mpwarfwk\Component\Controller\ControllerBase;

class SmartyController extends ControllerBase
{
	private $templateSystem;

	public function __construct(Container $container)
	{
		parent::__construct($container);
		$this->templateSystem = $this->container->getService('smarty_template');
	}

	public function index()
	{
		$this->templateSystem->assignVar('wine', 'Marques de Riscal');
		$response = new ResponseSmarty($this->templateSystem->createView('simpleWine.tpl'));

		return $response;
	}
}
