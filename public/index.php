<?php

use Symfony\Component\Yaml\Parser;
use Mpwarfwk\Component\Container\Container;
use Mpwarfwk\Component\Request;

require_once '../vendor/autoload.php';

error_reporting(-1);
ini_set("display_errors", 1);

define('ROOTPATH', realpath(__DIR__ . '/../'));

$parser = new Parser();
$container = new Container($parser, ROOTPATH . '/app/services', 'PROD');

$kernel = $container->getService('bootstrap');
$kernel->setContainer($container);

$request = Request::create();
$response = $kernel->execute($request);

$response->send();
